package com.codeiseasy.fbadsdk.pref

import android.content.Context
import android.content.SharedPreferences

class FbAdPrefs {
    var context: Context? = null
    var sharedPreferences: SharedPreferences? = null

    constructor(context: Context?){
        this.context = context
        this.sharedPreferences = this.context!!.getSharedPreferences(this::class.java.canonicalName, Context.MODE_PRIVATE)
    }

    fun writeBoolean(key: String?, value: Boolean) {
        this.sharedPreferences!!.edit().putBoolean(key.toString(), value).apply()
    }

    fun readBoolean(key: String?, default: Boolean) : Boolean {
        return this.sharedPreferences!!.getBoolean(key.toString(), default)
    }
}