package com.codeiseasy.fbadsdk.receiver

import android.content.Context
import android.content.Intent

interface NetworkStateReceiverCallback {
    fun onReceiver(context: Context?, intent: Intent?)
}