package com.codeiseasy.fbadsdk.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import java.lang.ref.WeakReference

class NetworkStateReceiver : BroadcastReceiver {
    private var context: WeakReference<Context>? = null
    private var callback: NetworkStateReceiverCallback? = null


    constructor(context: Context?) {
        this.context = WeakReference<Context>(context!!)
        this.listen(null)
    }

    override fun onReceive(p0: Context?, p1: Intent?) {
        Log.d("NETWORK_RECEIVER", "onReceive() intent($p1) action(${p1!!.action})")
        this.listen(p1)
    }

    private fun listen(intent: Intent?){
        this.callback?.onReceiver(context!!.get(), intent)
    }

    fun register(listener: NetworkStateReceiverCallback){
        this.callback = listener
        this.context?.get()?.registerReceiver(this, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    fun unregister(){
        try {
            this.context?.get()?.unregisterReceiver(this)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }
}