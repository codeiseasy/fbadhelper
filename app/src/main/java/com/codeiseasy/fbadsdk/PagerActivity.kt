package com.codeiseasy.fbadsdk

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.codeiseasy.fbadsdk.adapter.FragPagerAdapter

class PagerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pager_activity)

        var viewPager = findViewById<ViewPager>(R.id.view_pager)

        var fragPagerAdapter = FragPagerAdapter(supportFragmentManager)
        fragPagerAdapter.addFragment(MainFragment(), "Main Home")
        viewPager.adapter = fragPagerAdapter
        //tabLayout.setupWithViewPager(viewPager)
        viewPager.offscreenPageLimit = fragPagerAdapter.count
    }
}