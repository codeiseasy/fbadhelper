package com.codeiseasy.fbadsdk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.codeiseasy.fbadhelper.AudienceNetworkHelper
import com.codeiseasy.fbadhelper.format.FbAdBannerView
import com.codeiseasy.fbadhelper.format.FbAdNativeBannerView
import com.codeiseasy.fbadhelper.listener.FbAdListener
import com.codeiseasy.fbadhelper.listener.FbNativeAdListener
import com.codeiseasy.fbadsdk.receiver.NetworkStateReceiver

class MainFragment : Fragment() {
    private var networkStateReceiver: NetworkStateReceiver? = null
    private var audienceNetworkHelper: AudienceNetworkHelper? = null
    private var fbAdNativeBannerView: FbAdBannerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_media, container, false)
        fbAdNativeBannerView = rootView.findViewById(R.id.fb_banner_view)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        audienceNetworkHelper = AudienceNetworkHelper.Builder()
            .setContext(context)
            .setBannerUnitId(Constants.FACEBOOK_AD_BANNER_ID)
            .setInterstitialUnitId(Constants.FACEBOOK_AD_INTERSTISIAL_ID)
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorAccent)
            .setEnableAds(Constants.ENABLE_ADS)
            .build()

        audienceNetworkHelper?.loadAdView(fbAdNativeBannerView, object : FbAdListener {
            override fun onAdLoaded() {
                super.onAdLoaded()
                fbAdNativeBannerView?.visibility = View.VISIBLE
            }

            override fun onError(p1: String?) {
                super.onError(p1)
                fbAdNativeBannerView?.visibility = View.GONE
            }
        })
    }
}