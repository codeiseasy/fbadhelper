package com.codeiseasy.fbadsdk.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasy.fbadhelper.AudienceNetworkHelper
import com.codeiseasy.fbadhelper.format.FbAdBannerView
import com.codeiseasy.fbadhelper.listener.FbAdListener
import com.codeiseasy.fbadhelper.util.CheckNetwork
import com.codeiseasy.fbadsdk.Constants
import com.codeiseasy.fbadsdk.R
import com.codeiseasy.fbadsdk.pref.FbAdPrefs
import com.codeiseasy.fbadsdk.receiver.NetworkStateReceiver
import com.codeiseasy.fbadsdk.receiver.NetworkStateReceiverCallback
import com.facebook.ads.*

class Page2Activity : AppCompatActivity(), NetworkStateReceiverCallback {
    private var audienceNetworkHelper: AudienceNetworkHelper? = null
    private var networkStateReceiver: NetworkStateReceiver? = null
    private var fbAdPrefs: FbAdPrefs? = null
    private var fbBannerView: FbAdBannerView? = null
    private var fbParentAdView: RelativeLayout? = null

    override fun onReceiver(context: Context?, intent: Intent?) {
        if (CheckNetwork.getInstance(context).isOnline){

        } else {
            fbBannerView?.visibility = View.GONE
        }
    }

    private fun initBanner() {
        val adView = AdView(this, "IMG_16_9_APP_INSTALL#YOUR_PLACEMENT_ID", AdSize.BANNER_HEIGHT_50)
        val adListener = object : AdListener {
            override fun onAdLoaded(ad: Ad?) {

            }

            override fun onAdClicked(ad: Ad?) {

            }

            override fun onLoggingImpression(ad: Ad?) {

            }

            override fun onError(ad: Ad?, adError: AdError?) {

            }
        }
        fbParentAdView?.addView(adView)
        adView.loadAd(adView.buildLoadAdConfig().withAdListener(adListener).build())
    }

    private fun showAd() {
        initBanner()
        return
        fbBannerView?.visibility = View.VISIBLE
        audienceNetworkHelper?.loadAdView(fbBannerView, object : FbAdListener {
            override fun onAdLoaded() {
                super.onAdLoaded()
                fbBannerView?.visibility = View.VISIBLE
                Toast.makeText(applicationContext, "BannerView onAdLoaded()", Toast.LENGTH_LONG).show()
            }

            override fun onError(adError: String?) {
                super.onError(adError)
                Log.d("ERROR_LOAD_AD", adError.toString())
                fbBannerView?.visibility = View.GONE
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        networkStateReceiver = NetworkStateReceiver(this)
        networkStateReceiver?.register(this)

        fbAdPrefs = FbAdPrefs(this)

        fbBannerView = findViewById(R.id.fb_ad_banner_view)
        fbParentAdView = findViewById(R.id.parent_ad_view)

        findViewById<Button>(R.id.btn_next).setOnClickListener {
            startActivity(Intent(this, Page3Activity::class.java))
        }

        audienceNetworkHelper = AudienceNetworkHelper.Builder()
            .setContext(this)
            .setBannerUnitId(Constants.FACEBOOK_AD_BANNER_ID)
            .setInterstitialUnitId(Constants.FACEBOOK_AD_INTERSTISIAL_ID)
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorAccent)
            .setEnableAds(Constants.ENABLE_ADS)
            .setEnableBannerAd(fbAdPrefs!!.readBoolean("key_banner", true))
            .setEnableInterstitialAd(fbAdPrefs!!.readBoolean("key_interstitial", false))
            .setRetry(true)
            .build()

        showAd()
    }

    override fun onBackPressed() {
        finishAffinity()
    }

    override fun onDestroy() {
        super.onDestroy()
        audienceNetworkHelper?.destroy()
        networkStateReceiver?.unregister()
        fbBannerView?.destroy()
    }
}