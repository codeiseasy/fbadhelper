package com.codeiseasy.fbadsdk

import android.app.Application
import com.codeiseasy.fbadhelper.AudienceNetworkInitializeHelper

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // Initialize the Facebook Audience Network SDK
        AudienceNetworkInitializeHelper.initialize(this, BuildConfig.DEBUG, "ffffffff-9f4f-3bfc-ffff-ffff99d603a9")
    }
}