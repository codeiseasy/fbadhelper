package com.codeiseasy.fbadsdk

import android.Manifest.permission.READ_PHONE_STATE
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.res.ResourcesCompat
import com.codeiseasy.fbadhelper.AudienceNetworkHelper
import com.codeiseasy.fbadhelper.format.FanNativeTemplateStyle
import com.codeiseasy.fbadhelper.format.FbAdBannerView
import com.codeiseasy.fbadhelper.format.FbAdNativeBannerView
import com.codeiseasy.fbadhelper.format.FbAdNativeView
import com.codeiseasy.fbadhelper.listener.FbAdListener
import com.codeiseasy.fbadhelper.listener.FbInterstitialAdListener
import com.codeiseasy.fbadhelper.listener.FbNativeAdListener
import com.codeiseasy.fbadhelper.listener.FbRewardedVideoAdListener
import com.codeiseasy.fbadhelper.util.CheckNetwork
import com.codeiseasy.fbadsdk.activity.Page1Activity
import com.codeiseasy.fbadsdk.pref.FbAdPrefs
import com.codeiseasy.fbadsdk.receiver.NetworkStateReceiver
import com.codeiseasy.fbadsdk.receiver.NetworkStateReceiverCallback
import com.facebook.ads.*
import java.util.*

class MainActivity : AppCompatActivity(), NetworkStateReceiverCallback {
    private var audienceNetworkHelper: AudienceNetworkHelper? = null
    private var networkStateReceiver: NetworkStateReceiver? = null
    private var fbAdPrefs: FbAdPrefs? = null
    private lateinit var fbNativeView: FbAdNativeView
    private var fbBannerView: FbAdBannerView? = null
    private lateinit var fbNativeBannerView: FbAdNativeBannerView
    private lateinit var fbParentAdView: RelativeLayout

    companion object {
        const val BANNER = 1
        const val NATIVE_BANNER = 2
        const val NATIVE = 3
        const val INTERSTITIAL = 4
        const val REWARDED = 5
    }

    override fun onReceiver(context: Context?, intent: Intent?) {
        if (CheckNetwork.getInstance(context).isOnline){
            showAd(BANNER)
        } else {
            fbBannerView?.visibility = View.GONE
            fbNativeBannerView.visibility = View.GONE
            fbNativeView.visibility = View.GONE
        }
    }

    private fun initBanner() {
        val adView = AdView(this, Constants.FACEBOOK_AD_BANNER_ID, AdSize.BANNER_HEIGHT_50) // "IMG_16_9_APP_INSTALL#YOUR_PLACEMENT_ID"
        val adListener = object : AdListener {
            override fun onAdLoaded(ad: Ad?) {

            }

            override fun onAdClicked(ad: Ad?) {

            }

            override fun onLoggingImpression(ad: Ad?) {

            }

            override fun onError(ad: Ad?, adError: AdError?) {

            }
        }
        fbParentAdView.addView(adView)
        adView.loadAd(adView.buildLoadAdConfig().withAdListener(adListener).build())
    }

    private fun showAd(position: Int) {
        when(position) {
            BANNER -> {
                fbNativeBannerView.visibility = View.GONE
                fbNativeView.visibility = View.GONE

                initBanner()
               /* return

                fbBannerView?.visibility = View.VISIBLE

                audienceNetworkHelper?.loadAdView(fbBannerView, object : FbAdListener {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        fbBannerView?.visibility = View.VISIBLE
                        Toast.makeText(applicationContext, "BannerView onAdLoaded()", Toast.LENGTH_LONG).show()
                    }

                    override fun onError(adError: String?) {
                        super.onError(adError)
                        Log.d("ERROR_LOAD_AD", adError.toString())
                        fbBannerView?.visibility = View.GONE
                    }
                })*/
            }
            NATIVE_BANNER -> {
                fbBannerView?.visibility = View.GONE
                fbNativeView.visibility = View.GONE
                fbNativeBannerView.visibility = View.VISIBLE

                audienceNetworkHelper?.loadNativeBanner(
                    fbNativeBannerView,
                    object : FbNativeAdListener {
                        override fun onAdLoaded() {
                            super.onAdLoaded()
                            fbNativeBannerView.visibility = View.VISIBLE
                            Toast.makeText(applicationContext, "NativeBannerView onAdLoaded()", Toast.LENGTH_LONG).show()
                        }

                        override fun onError(adError: String?) {
                            super.onError(adError)
                            Log.d("ERROR_LOAD_AD", adError.toString())
                            fbNativeBannerView.visibility = View.GONE
                        }
                    })
            }
            NATIVE -> {
                fbBannerView?.visibility = View.GONE
                fbNativeBannerView.visibility = View.GONE
                fbNativeView.visibility = View.VISIBLE

                audienceNetworkHelper?.loadNativeAd(fbNativeView, object : FbNativeAdListener {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        fbNativeView.visibility = View.VISIBLE
                        Toast.makeText(applicationContext, "NativeView onAdLoaded()", Toast.LENGTH_LONG).show()
                    }
                    override fun onError(adError: String?) {
                        super.onError(adError)
                        Log.d("ERROR_LOAD_AD", adError.toString())
                        fbNativeView.visibility = View.GONE
                        Toast.makeText(applicationContext, adError.toString(), Toast.LENGTH_LONG).show()
                    }
                })
            }
            INTERSTITIAL -> audienceNetworkHelper?.loadInterstitialAd(object : FbInterstitialAdListener {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Toast.makeText(applicationContext, "Interstitial onAdLoaded()", Toast.LENGTH_LONG).show()
                }

                override fun onError(adError: String?) {
                    super.onError(adError)
                    Toast.makeText(applicationContext, adError.toString(), Toast.LENGTH_LONG).show()
                }
            })
            REWARDED -> audienceNetworkHelper?.loadRewardedVideoAd(object : FbRewardedVideoAdListener {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Toast.makeText(applicationContext, "Rewarded onAdLoaded()", Toast.LENGTH_LONG).show()
                }

                override fun onError(adError: String?) {
                    super.onError(adError)
                    Toast.makeText(applicationContext, adError.toString(), Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getDeviceId()
        networkStateReceiver = NetworkStateReceiver(this)
        networkStateReceiver?.register(this)

        fbAdPrefs = FbAdPrefs(this)

        fbNativeView = findViewById(R.id.fb_native_view)
        fbBannerView = findViewById(R.id.fb_ad_banner_view)
        fbNativeBannerView = findViewById(R.id.fb_native_banner_view)

        fbParentAdView = findViewById(R.id.parent_ad_view)
        findViewById<Button>(R.id.btn_next).setOnClickListener {
            startActivity(Intent(this, Page1Activity::class.java))
        }

        initAds()
    }

    private fun initAds() {
        audienceNetworkHelper = AudienceNetworkHelper.Builder()
            .setContext(this)
            .setBannerUnitId(Constants.FACEBOOK_AD_BANNER_ID)
            .setNativeUnitId(Constants.FACEBOOK_AD_NATIVE_ID)
            .setNativeBannerUnitId(Constants.FACEBOOK_AD_NATIVE_BANNER_ID)
            .setInterstitialUnitId(Constants.FACEBOOK_AD_INTERSTISIAL_ID)
            .setRewardedVideoUnitId(Constants.FACEBOOK_AD_REWARDED_VIDEO_ID)
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorAccent)
            .setEnableAds(Constants.ENABLE_ADS)
            .setNativeTemplateStyle(
                FanNativeTemplateStyle.Builder()
                    //.withMainBackgroundColor(ColorDrawable(Color.parseColor("#000000")))
                    .withTitleTextColor(Color.parseColor("#2B2B2B"))
                    .withTitleTextSize(25f)
                    .withTitleTextTypeface(ResourcesCompat.getFont(this, R.font.zahra_arabic_bold))
                    //.withCallToActionBackgroundColor(ColorDrawable(Color.parseColor("#DF3419")))
                    .build())
              .setEnableBannerAd(fbAdPrefs!!.readBoolean("key_banner", true))
              .setEnableNativeBannerAd(fbAdPrefs!!.readBoolean("key_native_banner", false))
              .setEnableNativeAd(fbAdPrefs!!.readBoolean("key_native", false))
              .setEnableInterstitialAd(fbAdPrefs!!.readBoolean("key_interstitial", false))
              .setEnableRewardedVideoAd(fbAdPrefs!!.readBoolean("key_rewarded", false))
            .build()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_fb_settings -> dialogSettings()

            R.id.item_fb_banner -> showAd(BANNER)

            R.id.item_fb_native_banner -> showAd(NATIVE_BANNER)

            R.id.item_fb_native -> showAd(NATIVE)

            R.id.item_fb_interstitial -> showAd(INTERSTITIAL)

            R.id.item_fb_rewarded -> showAd(REWARDED)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun dialogSettings() {
        val rootView = LayoutInflater.from(this).inflate(R.layout.fb_ad_settings, null)
        val switchEnableBanner = rootView.findViewById<SwitchCompat>(R.id.switch_enable_banner)
        val switchEnableNativeBanner = rootView.findViewById<SwitchCompat>(R.id.switch_enable_native_banner)
        val switchEnableNative = rootView.findViewById<SwitchCompat>(R.id.switch_enable_native)
        val switchEnableInterstitial = rootView.findViewById<SwitchCompat>(R.id.switch_enable_interstitial)
        val switchEnableRewarded = rootView.findViewById<SwitchCompat>(R.id.switch_enable_rewarded)

        switchEnableBanner.isChecked = fbAdPrefs!!.readBoolean("key_banner", true)
        switchEnableNativeBanner.isChecked = fbAdPrefs!!.readBoolean("key_native_banner", false)
        switchEnableNative.isChecked = fbAdPrefs!!.readBoolean("key_native", false)
        switchEnableInterstitial.isChecked = fbAdPrefs!!.readBoolean("key_interstitial", false)
        switchEnableRewarded.isChecked = fbAdPrefs!!.readBoolean("key_rewarded", false)

        AlertDialog.Builder(this)
            .setTitle(resources.getString(R.string.ad_settings_label))
            .setView(rootView)
            .setPositiveButton(resources.getString(R.string.btn_apply)) { dialog, _ ->
                dialog.dismiss()
                fbAdPrefs?.writeBoolean("key_banner", switchEnableBanner.isChecked)
                fbAdPrefs?.writeBoolean("key_native_banner", switchEnableNativeBanner.isChecked)
                fbAdPrefs?.writeBoolean("key_native", switchEnableNative.isChecked)
                fbAdPrefs?.writeBoolean("key_interstitial", switchEnableInterstitial.isChecked)
                fbAdPrefs?.writeBoolean("key_rewarded", switchEnableRewarded.isChecked)
                recreate()
            }
            .setNegativeButton(resources.getString(R.string.btn_cancel), null)
            .show()
    }

    override fun onBackPressed() {
        //startActivity(Intent(this, PagerActivity::class.java))
        finishAffinity()
    }

    override fun onDestroy() {
        super.onDestroy()
        audienceNetworkHelper?.destroy()
        networkStateReceiver?.unregister()
        fbBannerView?.destroy()
        fbNativeBannerView.destroy()
        fbNativeView.destroy()
    }

    @Suppress("deprecation")
    private fun getDeviceId() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                val tm: TelephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                val tmDevice: String = "" + tm.deviceId
                val tmSerial: String = "" + tm.simSerialNumber
                val androidId: String = "" + Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
                val deviceUuid = UUID(androidId.hashCode().toLong(), tmDevice.hashCode().toLong() shl 32 or tmSerial.hashCode().toLong())
                val deviceId: String = deviceUuid.toString()
                Log.d("DEVICE_ID_INFO", deviceId)
            }
        }
    }
}