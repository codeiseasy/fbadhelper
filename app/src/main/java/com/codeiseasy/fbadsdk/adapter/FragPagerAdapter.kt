package com.codeiseasy.fbadsdk.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class FragPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    private var fragments: ArrayList<Fragment> = ArrayList()
    private var titles: ArrayList<String> = ArrayList()


    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return titles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return titles[position]
    }


    fun addFragment(fragment: Fragment){
        fragments.add(fragment)
    }

    fun addTitle(title: String){
        titles.add(title)
    }

    fun addFragment(fragment: Fragment, title: String){
        fragments.add(fragment)
        titles.add(title)
        notifyDataSetChanged()
    }
}