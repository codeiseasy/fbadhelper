package com.codeiseasy.fbadhelper.prefs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by NoThing on 6/12/2018.
 */

public class FbAdPreferences {
    public SharedPreferences preferences;
    public Context context;

    public FbAdPreferences(Context context) {
        this.context = context;
    }
    
    public void setPreferences(boolean isClass) {
        preferences = context.getSharedPreferences("class_name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("is_class_full_screen", isClass);
        editor.apply();
    }

    public boolean getPreferences(){
        return context.getSharedPreferences("class_name", Context.MODE_PRIVATE).getBoolean("is_class_full_screen", false);
    }

    public void setDownloadPrefReloadLeftDrawer(int intiger) {
        preferences = context.getSharedPreferences("download_count_total", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("is_download_count_big", intiger);
        editor.apply();
    }

    public int getDownloadPrefReloadLeftDrawer(){
        return preferences.getInt("is_download_count_big", 0);
    }

    public void setFavoritePrefReloadLeftDrawer(int intiger) {
        preferences = context.getSharedPreferences("download_count_total", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("is_download_count_big", intiger);
        editor.apply();
    }

    public int getFavoritePrefReloadLeftDrawer(){
        return preferences.getInt("is_download_count_big", 0);
    }

    public void setAdLoadedCountPref(int intiger) {
        preferences = context.getSharedPreferences("adloaded_count_total", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("is_adloaded_count", intiger);
        editor.apply();
    }

    public int getAdLoadedCountPref(){
        return context.getSharedPreferences("adloaded_count_total", Context.MODE_PRIVATE).getInt("is_adloaded_count", 0);
    }

    public void setAdBannerPref(boolean b) {
        preferences = context.getSharedPreferences("adbanner_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("is_adbanner_boolean", b);
        editor.apply();
    }

    public boolean getAdBannerPref(){
        return context.getSharedPreferences("adbanner_pref", Context.MODE_PRIVATE).getBoolean("is_adbanner_boolean", false);
    }

    public void setAdBannerForLifePref(boolean b) {
        preferences = context.getSharedPreferences("adbanner_for_life_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("is_adbanner_for_life_boolean", b);
        editor.apply();
    }

    public boolean getAdBannerForLifePref(){
        return context.getSharedPreferences("adbanner_for_life_pref", Context.MODE_PRIVATE).getBoolean("is_adbanner_for_life_boolean", false);
    }

    // setupShape Ad Count
    public void setAdCountPref(String key, int value) {
        preferences = context.getSharedPreferences("ad_count_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    // get Ad Count
    public int getAdCountPref(String key){
        return context.getSharedPreferences("ad_count_pref", Context.MODE_PRIVATE).getInt(key, 1);
    }

    //tvPenEraseSize
    public void setPenEraseSizePref(int value) {
        preferences = context.getSharedPreferences("tvPenEraseSize_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("tvPenEraseSize_key", value);
        editor.apply();
    }

    public int getPenEraseSizePref(){
        return context.getSharedPreferences("tvPenEraseSize_pref", Context.MODE_PRIVATE).getInt("tvPenEraseSize", 0);
    }


    // setupShape Ad Count
    public void setSavedFilesCountPref(String key, int value) {
        preferences = context.getSharedPreferences("files_count_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    // get Ad Count
    public int getSavedFilesCountPref(String key){
        return context.getSharedPreferences("files_count_pref", Context.MODE_PRIVATE).getInt(key, 0);
    }
}
