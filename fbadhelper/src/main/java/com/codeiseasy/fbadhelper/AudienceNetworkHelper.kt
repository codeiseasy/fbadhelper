package com.codeiseasy.fbadhelper


import android.content.Context
import android.graphics.Color

import android.util.Log
import android.view.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.codeiseasy.fbadhelper.dialog.UILoadingDialog
import com.codeiseasy.fbadhelper.format.FanNativeTemplateStyle
import com.codeiseasy.fbadhelper.format.FbAdBannerView
import com.codeiseasy.fbadhelper.format.FbAdNativeBannerView
import com.codeiseasy.fbadhelper.format.FbAdNativeView
import com.codeiseasy.fbadhelper.listener.FbAdListener
import com.codeiseasy.fbadhelper.listener.FbInterstitialAdListener
import com.codeiseasy.fbadhelper.listener.FbNativeAdListener
import com.codeiseasy.fbadhelper.listener.FbRewardedVideoAdListener
import com.codeiseasy.fbadhelper.prefs.FbAdPreferences

import com.codeiseasy.fbadhelper.util.CheckNetwork
import com.codeiseasy.fbadhelper.util.IndicatorHelper.loadingAnimationType
import com.codeiseasy.fbadhelper.util.FbAdLog
import com.codeiseasy.fbadhelper.util.TextUtils
import com.wang.avi.AVLoadingIndicatorView
import java.util.*
import com.facebook.ads.*

class AudienceNetworkHelper {
    private var context: Context? = null

    private constructor(context: Context?) {
        this.context = context
    }

    companion object {
        private val TAG = AudienceNetworkHelper::class.java.name
        private const val MAX_NUMBER_OF_RETRIES = 3
    }

    private var fbInterstitialAd: InterstitialAd? = null
    private var fbRewardedVideoAd: RewardedVideoAd? = null
    private var loadingDialog: UILoadingDialog? = null
    private var animationAccentColor: Int = android.R.color.holo_orange_dark
    private var isLoadAnimationBeforeLoadAd: Boolean = true

    private var fbBannerAdView: AdView? = null
    private var fbBannerAdSize: AdSize = AdSize.BANNER_HEIGHT_50

    private var fbBannerPlacementId: String? = null
    private var fbNativeBannerPlacementId: String? = null
    private var fbNativePlacementId: String? = null
    private var fbInterstitialPlacementId: String? = null
    private var fbRewardedVideoPlacementId: String? = null

    private var isTestModeAds: Boolean = false
    private var isEnableAds: Boolean = true
    private var isEnableBannerAd: Boolean = true
    private var isEnableNativeBannerAd: Boolean = true
    private var isEnableNativeAd: Boolean = true
    private var isEnableInterstitialAd: Boolean = true
    private var isEnableRewardedVideoAd: Boolean = true

    private var fbAdPreferences: FbAdPreferences? = null
    private var fanNativeTemplateStyle: FanNativeTemplateStyle? = null

    private var isRetryLoadAd: Boolean = false
    private var retryCount: Int = 0

    private fun init() {
        this.fbAdPreferences = FbAdPreferences(context)

        var loadingView = LayoutInflater.from(context).inflate(R.layout.ui_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.av_loading_indicator_view)
        avLoadingIndicatorView.setIndicatorColor(
            ContextCompat.getColor(
                context!!,
                animationAccentColor
            )
        )
        avLoadingIndicatorView.setIndicator(
            loadingAnimationType[Random().nextInt(
                loadingAnimationType.size
            )]
        )

        var linearLayout = LinearLayout(context)
        var textView = TextView(context)

        var linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearParams.gravity = Gravity.CENTER
        linearLayout.layoutParams = linearParams
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER

        var textParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        textParams.topMargin = 10
        textView.layoutParams = textParams
        textView.text = "Loading Ad..."
        textView.setTextColor(Color.WHITE)
        textView.setShadowLayer(2f, -1f, -1f, Color.GRAY)
        textView.textSize = 18f
        textView.isAllCaps = true

        linearLayout.addView(loadingView)
        linearLayout.addView(textView)

        this.loadingDialog = UILoadingDialog.init(context)
            .cancelable(false)
            .backgroundColor(android.R.color.white)
            .view(linearLayout)
            .build()
    }

    fun build(): AudienceNetworkHelper {
        init()
        return this
    }

    fun setBannerUnitId(unitId: String?): AudienceNetworkHelper {
        this.fbBannerPlacementId = unitId
        return this
    }

    fun setNativeBannerUnitId(unitId: String?): AudienceNetworkHelper {
        this.fbNativeBannerPlacementId = unitId
        return this
    }

    fun setNativeUnitId(unitId: String?): AudienceNetworkHelper {
        this.fbNativePlacementId = unitId
        return this
    }

    fun setInterstitialUnitId(unitId: String?): AudienceNetworkHelper {
        this.fbInterstitialPlacementId = unitId
        return this
    }

    fun setRewardedVideoUnitId(unitId: String?): AudienceNetworkHelper {
        this.fbRewardedVideoPlacementId = unitId
        return this
    }

    fun setAdViewSize(adSize: AdSize) : AudienceNetworkHelper {
        this.fbBannerAdSize = adSize
        return this
    }

    fun setLoadAnimationBeforeLoadAd(anim: Boolean): AudienceNetworkHelper {
        this.isLoadAnimationBeforeLoadAd = anim
        return this
    }

    fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): AudienceNetworkHelper {
        this.animationAccentColor = color
        return this
    }

    fun setTestMode(testMode: Boolean) : AudienceNetworkHelper {
        this.isTestModeAds = testMode
        return this
    }

    fun setEnableAds(enable: Boolean): AudienceNetworkHelper {
        this.isEnableAds = enable
        return this
    }

    fun setEnableBannerAd(enable: Boolean): AudienceNetworkHelper {
        this.isEnableBannerAd = enable
        //this.isEnableAds = enable
        return this
    }

    fun setEnableNativeBannerAd(enable: Boolean): AudienceNetworkHelper {
        this.isEnableNativeBannerAd = enable
        //this.isEnableAds = enable
        return this
    }

    fun setEnableNativeAd(enable: Boolean): AudienceNetworkHelper {
        this.isEnableNativeAd = enable
        //this.isEnableAds = enable
        return this
    }

    fun setEnableInterstitialAd(enable: Boolean): AudienceNetworkHelper {
        this.isEnableInterstitialAd = enable
        //this.isEnableAds = enable
        return this
    }

    fun setEnableRewardedVideoAd(enable: Boolean): AudienceNetworkHelper {
        this.isEnableRewardedVideoAd = enable
        //this.isEnableAds = enable
        return this
    }

    fun setNativeTemplateStyle(styles: FanNativeTemplateStyle?): AudienceNetworkHelper {
        this.fanNativeTemplateStyle = styles
        return this
    }

    @Deprecated("")
    fun setRetry(retry: Boolean): AudienceNetworkHelper {
        this.isRetryLoadAd = retry
        return this
    }

    fun loadAdView(fbAdBannerView: FbAdBannerView) {
        this.loadBannerIfEnabled(fbAdBannerView, null)
    }

    fun loadAdView(fbAdBannerView: FbAdBannerView?, listener: FbAdListener?) {
        this.loadBannerIfEnabled(fbAdBannerView, listener)
    }

    fun loadAdView(viewAd: LinearLayout?) {
        this.loadBannerIfEnabled(viewAd, null)
    }

    fun loadAdView(viewAd: LinearLayout, listener: FbAdListener?) {
        this.loadBannerIfEnabled(viewAd, listener)
    }

    fun loadAdView(viewAd: RelativeLayout?) {
        this.loadBannerIfEnabled(viewAd, null)
    }

    fun loadAdView(viewAd: RelativeLayout, listener: FbAdListener?) {
        this.loadBannerIfEnabled(viewAd, listener)
    }

    fun loadAdView(viewAd: FrameLayout?) {
        this.loadBannerIfEnabled(viewAd, null)
    }

    fun loadAdView(viewAd: FrameLayout, listener: FbAdListener?) {
        this.loadBannerIfEnabled(viewAd, listener)
    }

    fun loadNativeBanner(nativeBannerView: FbAdNativeBannerView?) {
        this.loadNativeBannerIfEnabled(nativeBannerView, null)
    }

    fun loadNativeBanner(nativeBannerView: FbAdNativeBannerView, listener: FbNativeAdListener?) {
        this.loadNativeBannerIfEnabled(nativeBannerView, listener)
    }

    fun loadNativeAd(viewAd: FbAdNativeView) {
        this.loaAdNativeIfEnabled(viewAd, null)
    }

    fun loadNativeAd(viewAd: FbAdNativeView, listener: FbNativeAdListener?) {
        this.loaAdNativeIfEnabled(viewAd, listener)
    }

    private fun loadBannerIfEnabled(bannerView: FbAdBannerView?, listener: FbAdListener?) {
        if (CheckNetwork.getInstance(context).isOnline) {
            //Log.d("CHECK_AD_ENABLED",  "$isEnableAds \n $isEnableBannerAd")
            if (isEnableAds) {
                if (isEnableBannerAd) {
                    loadBanner(bannerView, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Banner"))
                }
            } else {
                if (isEnableBannerAd) {
                    loadBanner(bannerView, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Banner"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadBannerIfEnabled(viewAd: ViewGroup?, listener: FbAdListener?) {
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableBannerAd) {
                    loadBanner(viewAd, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Banner"))
                }
            } else {
                if (isEnableBannerAd) {
                    loadBanner(viewAd, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Banner"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadBanner(bannerView: FbAdBannerView?, listener: FbAdListener?) {
        if (TextUtils.isEmpty(this.fbBannerPlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on BannerUnitId before loadAd is called.")
        }
        if (this.isTestModeAds){
            this.fbBannerPlacementId = "IMG_16_9_APP_INSTALL#${this.fbBannerPlacementId.toString()}"
        }
        Log.d("BANNER_PLACEMENT_ID", this.fbBannerPlacementId.toString())

        bannerView?.let {
            it.setBannerPlacementId(this.fbBannerPlacementId)
            it.setBannerAdViewSize(this.fbBannerAdSize)
            //it.setAdView(this.fbBannerAdView)
            it.setRetry(this.isRetryLoadAd)
            it.adListener(listener)
            it.loadAd()
        }
    }

    private fun loadBanner(viewGroup: ViewGroup?, listener: FbAdListener?) {
        if (TextUtils.isEmpty(this.fbBannerPlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on BannerUnitId before loadAd is called.")
        }
        if (this.isTestModeAds){
            this.fbBannerPlacementId = "IMG_16_9_APP_INSTALL#${this.fbBannerPlacementId.toString()}"
        }
        val adListener = object : AdListener {
            override fun onAdLoaded(ad: Ad?) {
                listener?.onAdLoaded()
            }

            override fun onAdClicked(ad: Ad?) {
                listener?.onAdClicked()
            }

            override fun onLoggingImpression(ad: Ad?) {
                listener?.onLoggingImpression()
            }

            override fun onError(ad: Ad?, adError: AdError?) {
                listener?.onError(adError!!.errorMessage)
                Log.d("FB_BANNER_ERROR", adError!!.errorMessage)
            }
        }
        Log.d("BANNER_PLACEMENT_ID", this.fbBannerPlacementId.toString())
        this.fbBannerAdView = AdView(this.context, this.fbBannerPlacementId, this.fbBannerAdSize)
        fbBannerAdView?.let { nonNullBannerAdView ->
            viewGroup?.removeAllViews()
            viewGroup?.addView(nonNullBannerAdView)
            nonNullBannerAdView.loadAd(nonNullBannerAdView.buildLoadAdConfig().withAdListener(adListener).build())

        }
    }

    private fun loadNativeBannerIfEnabled(nativeBannerView: FbAdNativeBannerView?, listener: FbNativeAdListener?) {
        if (CheckNetwork.getInstance(this.context).isOnline) {
            if (isEnableAds) {
                if (isEnableNativeBannerAd) {
                    loadNativeBannerAd(nativeBannerView, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Native banner"))
                }
            } else {
                if (isEnableNativeBannerAd) {
                    loadNativeBannerAd(nativeBannerView, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Native banner"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }

    }

    private fun loadNativeBannerAd(nativeBannerView: FbAdNativeBannerView?, listener: FbNativeAdListener?) {
        if (TextUtils.isEmpty(this.fbNativeBannerPlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeBannerUnitId before loadAd is called.")
        }
        nativeBannerView?.setAdUnitId(this.fbNativeBannerPlacementId.toString())
        nativeBannerView?.setRetry(isRetryLoadAd)
        nativeBannerView?.setTemplateStyle(fanNativeTemplateStyle)
        nativeBannerView?.adListener(listener)
        nativeBannerView?.loadAd()
    }

    private fun loaAdNativeIfEnabled(nativeVew: FbAdNativeView, listener: FbNativeAdListener?) {
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableNativeAd) {
                    loaAdNativeWithListener(nativeVew, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Native"))
                }
            } else {
                if (isEnableNativeAd) {
                    loaAdNativeWithListener(nativeVew, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Native"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loaAdNativeWithListener(nativeVew: FbAdNativeView, listener: FbNativeAdListener?) {
        if (TextUtils.isEmpty(this.fbNativePlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeUnitId before loadAd is called.")
        }
        nativeVew.setAdUnitId(this.fbNativePlacementId)
        nativeVew.setTemplateStyle(fanNativeTemplateStyle)
        nativeVew.setRetry(isRetryLoadAd)
        nativeVew.adListener(listener)
        nativeVew.loadAd()
    }

    fun loadInterstitialAd() {
        this.loadInterstitialIfEnabled(null)
    }

    fun loadInterstitialAd(listener: FbInterstitialAdListener?) {
        this.loadInterstitialIfEnabled(listener)
    }

    fun loadInterstitialAfterEndCount(view: View?, count: Int) {
        var key = view!!.id.toString()
        this.loadInterstitialAfterEndCountWithListener(key, count, null)
    }

    fun loadInterstitialAfterEndCount(key: String?, count: Int) {
        this.loadInterstitialAfterEndCountWithListener(key, count, null)
    }

    fun loadInterstitialAfterEndCount(view: View?, count: Int, listener: FbInterstitialAdListener?) {
        var key = view!!.id.toString()
        this.loadInterstitialAfterEndCountWithListener(key, count, listener)
    }

    fun loadInterstitialAfterEndCount(key: String?, count: Int, listener: FbInterstitialAdListener?) {
        this.loadInterstitialAfterEndCountWithListener(key, count, listener)
    }

    private fun loadInterstitialAfterEndCountWithListener(key: String?, count: Int, listener: FbInterstitialAdListener?) {
        if (this.fbInterstitialPlacementId.isNullOrEmpty()) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableInterstitialAd) {
                    loadInterstitialAfterEndCountIfEnabled(key, count, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Interstitial"))
                }
            } else {
                if (isEnableInterstitialAd) {
                    loadInterstitialAfterEndCountIfEnabled(key, count, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Interstitial"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadInterstitialIfEnabled(listener: FbInterstitialAdListener?) {
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableInterstitialAd) {
                    this.loadInterstitialAdWithListener(listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Interstitial"))
                }
            } else {
                if (isEnableInterstitialAd) {
                    this.loadInterstitialAdWithListener(listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Interstitial"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadInterstitialAfterEndCountIfEnabled(key: String?, count: Int, listener: FbInterstitialAdListener?) {
        var prefKey = "interstitial_" + key.toString()
        var totalCount = doCountInterstitialAd(prefKey)
        if (this.fbAdPreferences!!.getAdCountPref(prefKey) == count) {
            this.fbAdPreferences?.setAdCountPref(prefKey, 1)
            this.loadInterstitialAdWithListener(listener)
        } else {
            this.fbAdPreferences?.setAdCountPref(prefKey, totalCount)
            listener?.onInterstitialDismissed()
        }
        Log.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun loadInterstitialAdWithListener(listener: FbInterstitialAdListener?) {
        if (TextUtils.isEmpty(this.fbInterstitialPlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        this.fbInterstitialAd = InterstitialAd(this.context, this.fbInterstitialPlacementId)
        var adListener = object : InterstitialAdListener {
            override fun onInterstitialDisplayed(ad: Ad?) { // Interstitial ad displayed callback
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog?.hide()
                }
                listener?.onInterstitialDisplayed()
            }

            override fun onInterstitialDismissed(ad: Ad) { // Interstitial dismissed callback
                listener?.onInterstitialDismissed()
            }

            override fun onError(ad: Ad?, adError: AdError?) { // Ad error callback
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog?.hide()
                }
                listener?.onError("Ad failed to load: " +  adError!!.errorMessage)
            }

            override fun onAdLoaded(ad: Ad) { // Interstitial ad is loaded and ready to be displayed
                FbAdLog.d(TAG, "Interstitial ad is loaded and ready to be displayed!")
                // Show the ad
                listener?.onAdLoaded()
                if (fbInterstitialAd!!.isAdLoaded) {
                    fbInterstitialAd?.show()
                }
            }

            override fun onAdClicked(ad: Ad) { // Ad clicked callback
                FbAdLog.d(TAG, "Interstitial ad clicked!")
                listener?.onAdClicked()
            }

            override fun onLoggingImpression(ad: Ad) { // Ad impression logged callback
                FbAdLog.d(TAG, "Interstitial ad impression logged!")
                listener?.onLoggingImpression()
            }
        }
        if (isLoadAnimationBeforeLoadAd) {
            this.loadingDialog?.show()
        }
        // Set listeners for the Interstitial Ad
        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        fbInterstitialAd?.let { nonNullInterstitial ->
            nonNullInterstitial.loadAd(nonNullInterstitial.buildLoadAdConfig().withAdListener(adListener).build())
        }
    }

    fun loadRewardedVideoAd() {
        this.loadRewardedVideoIfEnabled(null)
    }

    fun loadRewardedVideoAd(listener: FbRewardedVideoAdListener?) {
        this.loadRewardedVideoIfEnabled(listener)
    }

    fun loadRewardedVideoAfterEndCount(view: View?, count: Int) {
        var key = view!!.id.toString()
        this.loadRewardedVideoAfterEndCountWithListener(key, count, null)
    }

    fun loadRewardedVideoAfterEndCount(key: String?, count: Int) {
        this.loadRewardedVideoAfterEndCountWithListener(key, count, null)
    }

    fun loadRewardedVideoAfterEndCount(view: View?, count: Int, listener: FbRewardedVideoAdListener?) {
        var key = view!!.id.toString()
        this.loadRewardedVideoAfterEndCountWithListener(key, count, listener)
    }

    fun loadRewardedVideoAfterEndCount(key: String?, count: Int, listener: FbRewardedVideoAdListener?) {
        this.loadRewardedVideoAfterEndCountWithListener(key, count, listener)
    }

    private fun loadRewardedVideoAfterEndCountWithListener(key: String?, count: Int, listener: FbRewardedVideoAdListener?) {
        if (this.fbInterstitialPlacementId.isNullOrEmpty()) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableInterstitialAd) {
                    loadRewardedVideoAfterEndCountIfEnabled(key, count, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Rewarded"))
                }
            } else {
                if (isEnableInterstitialAd) {
                    loadRewardedVideoAfterEndCountIfEnabled(key, count, listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Rewarded"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadRewardedVideoIfEnabled(listener: FbRewardedVideoAdListener?) {
        if (CheckNetwork.getInstance(context).isOnline) {
            if (isEnableAds) {
                if (isEnableRewardedVideoAd) {
                    this.loadRewardedVideoAdWithListener(listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Rewarded"))
                }
            } else {
                if (isEnableRewardedVideoAd) {
                    this.loadRewardedVideoAdWithListener(listener)
                } else {
                    listener?.onError(String.format(context!!.resources.getString(R.string.no_enabled_ads), "Rewarded"))
                }
            }
        } else {
            listener?.onError(context!!.resources.getString(R.string.no_internet_access))
        }
    }

    private fun loadRewardedVideoAfterEndCountIfEnabled(key: String?, count: Int, listener: FbRewardedVideoAdListener?) {
        var prefKey = "rewarded_video_" + key.toString()
        var totalCount = doCountRewardedVideoAd(prefKey)
        if (this.fbAdPreferences!!.getAdCountPref(prefKey) == count) {
            this.fbAdPreferences?.setAdCountPref(prefKey, 1)
            this.loadRewardedVideoAdWithListener(listener)
        } else {
            this.fbAdPreferences?.setAdCountPref(prefKey, totalCount)
            listener?.onRewardedVideoClosed()
        }
        Log.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun loadRewardedVideoAdWithListener(listener: FbRewardedVideoAdListener?) {
        if (TextUtils.isEmpty(this.fbRewardedVideoPlacementId)) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        this.fbRewardedVideoAd = RewardedVideoAd(this.context, this.fbRewardedVideoPlacementId)
        var adListener = object : RewardedVideoAdListener {
            override fun onAdLoaded(p0: Ad?) {
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog?.hide()
                }
                if (fbRewardedVideoAd!!.isAdLoaded) {
                    fbRewardedVideoAd?.show()
                }
                listener?.onAdLoaded()
            }

            override fun onAdClicked(p0: Ad?) {
                listener?.onAdClicked()
            }

            override fun onRewardedVideoClosed() {
                listener?.onRewardedVideoClosed()
            }

            override fun onRewardedVideoCompleted() {
                listener?.onRewardedVideoCompleted()
            }

            override fun onLoggingImpression(p0: Ad?) {
                listener?.onLoggingImpression()
            }

            override fun onError(p0: Ad?, adError: AdError?) {
                if (isLoadAnimationBeforeLoadAd) {
                    loadingDialog?.hide()
                }
                listener?.onError("Ad failed to load: " + adError!!.errorMessage)
            }
        }
        // Set listeners for the Interstitial Ad
        // For auto play video ads, it's recommended to load the ad
        // at least 30 seconds before it is shown
        fbRewardedVideoAd?.let { nonNullFbRewardedVideoAd ->
            if (isLoadAnimationBeforeLoadAd) {
                this.loadingDialog?.show()
            }
            nonNullFbRewardedVideoAd.loadAd(nonNullFbRewardedVideoAd.buildLoadAdConfig().withAdListener(adListener).build())
        }
    }

    @Deprecated("")
    fun loadNativeAdScrollAd(view: ViewGroup, placementId: String?) {
        var manager = NativeAdsManager(context, placementId, MAX_NUMBER_OF_RETRIES)
        manager.setListener(object : NativeAdsManager.Listener {
            override fun onAdsLoaded() {
                val nativeAdScrollView = NativeAdScrollView(context, manager, 300)
                view.addView(nativeAdScrollView)
            }

            override fun onAdError(p0: AdError?) {

            }
        })
        manager.loadAds()
    }

    private fun doCountInterstitialAd(key: String?): Int {
        return this.fbAdPreferences!!.getAdCountPref(key) + 1
    }

    private fun doCountRewardedVideoAd(key: String?): Int {
        return this.fbAdPreferences!!.getAdCountPref(key) + 1
    }

    class Builder {
        private var context: Context? = null
        private var adBannerUnitId: String? = null
        private var adNativeBannerUnitId: String? = null
        private var adNativeUnitId: String? = null
        private var adInterstitialUnitId: String? = null
        private var adRewardedVideoUnitId: String? = null

        private var isTestModeAds: Boolean = false
        private var isEnableAds: Boolean = true
        private var isEnableBannerAd: Boolean = true
        private var isEnableNativeBannerAd: Boolean = true
        private var isEnableNativeAd: Boolean = true
        private var isEnableInterstitialAd: Boolean = true
        private var isEnableRewardedVideoAd: Boolean = true

        private var isRetryLoadAd: Boolean = false
        private var fanNativeTemplateStyle: FanNativeTemplateStyle? = null

        private var isLoadAnimationBeforeLoadAd: Boolean = false
        private var animationAccentColor: Int = android.R.color.holo_orange_dark

        fun setContext(context: Context?): Builder {
            this.context = context
            return this
        }

        fun setBannerUnitId(unitId: String?): Builder {
            this.adBannerUnitId = unitId
            return this
        }

        fun setNativeBannerUnitId(unitId: String?): Builder {
            this.adNativeBannerUnitId = unitId
            return this
        }

        fun setNativeUnitId(unitId: String?): Builder {
            this.adNativeUnitId = unitId
            return this
        }

        fun setInterstitialUnitId(unitId: String?): Builder {
            this.adInterstitialUnitId = unitId
            return this
        }

        fun setRewardedVideoUnitId(unitId: String?): Builder {
            this.adRewardedVideoUnitId = unitId
            return this
        }

        fun setLoadAnimationBeforeLoadAd(anim: Boolean): Builder {
            this.isLoadAnimationBeforeLoadAd = anim
            return this
        }

        fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): Builder {
            this.animationAccentColor = color
            return this
        }

        fun setTestMode(testMode: Boolean) : Builder {
            this.isTestModeAds = testMode
            return this
        }

        fun setEnableAds(enable: Boolean): Builder {
            this.isEnableAds = enable
            return this
        }

        @Deprecated("")
        fun setRetry(retry: Boolean): Builder {
            this.isRetryLoadAd = retry
            return this
        }

        fun setEnableBannerAd(enable: Boolean): Builder {
            this.isEnableBannerAd = enable
            //this.isEnableAds = enable
            return this
        }

        fun setEnableNativeBannerAd(enable: Boolean): Builder {
            this.isEnableNativeBannerAd = enable
            //this.isEnableAds = enable
            return this
        }

        fun setEnableNativeAd(enable: Boolean): Builder {
            this.isEnableNativeAd = enable
            //this.isEnableAds = enable
            return this
        }

        fun setEnableInterstitialAd(enable: Boolean): Builder {
            this.isEnableInterstitialAd = enable
            //this.isEnableAds = enable
            return this
        }

        fun setEnableRewardedVideoAd(enable: Boolean): Builder {
            this.isEnableRewardedVideoAd = enable
            //this.isEnableAds = enable
            return this
        }

        fun setNativeTemplateStyle(styles: FanNativeTemplateStyle?): Builder {
            this.fanNativeTemplateStyle = styles
            return this
        }

        fun build(): AudienceNetworkHelper {
            return AudienceNetworkHelper(this.context)
                .setBannerUnitId(this.adBannerUnitId)
                .setNativeBannerUnitId(this.adNativeBannerUnitId)
                .setNativeUnitId(this.adNativeUnitId)
                .setInterstitialUnitId(this.adInterstitialUnitId)
                .setRewardedVideoUnitId(this.adRewardedVideoUnitId)
                .setLoadAnimationBeforeLoadAd(this.isLoadAnimationBeforeLoadAd)
                .setLoadAnimationBeforeLoadAdAccentColor(this.animationAccentColor)
                .setTestMode(this.isTestModeAds)
                .setEnableAds(this.isEnableAds)
                .setRetry(this.isRetryLoadAd)
                .setEnableBannerAd(this.isEnableBannerAd)
                .setEnableNativeBannerAd(this.isEnableNativeBannerAd)
                .setEnableNativeAd(this.isEnableNativeAd)
                .setEnableInterstitialAd(this.isEnableInterstitialAd)
                .setEnableRewardedVideoAd(this.isEnableRewardedVideoAd)
                .setNativeTemplateStyle(this.fanNativeTemplateStyle)
                .build()
        }
    }

    fun destroy() {
        if (fbInterstitialAd != null) {
            fbInterstitialAd?.destroy()
            fbInterstitialAd = null
        }
        if (fbRewardedVideoAd != null) {
            fbRewardedVideoAd?.destroy()
            fbRewardedVideoAd = null
        }
    }
}