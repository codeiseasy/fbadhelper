package com.codeiseasy.fbadhelper

import android.content.Context
import android.util.Log
import com.codeiseasy.fbadhelper.util.TextUtils.isNotEmpty
import com.facebook.ads.AdSettings
import com.facebook.ads.AudienceNetworkAds
import com.facebook.ads.AudienceNetworkAds.InitListener
import com.facebook.ads.AudienceNetworkAds.InitResult

/**
 * Sample class that shows how to call initialize() method of Audience Network SDK.
 */
class AudienceNetworkInitializeHelper : InitListener {


    override fun onInitialized(result: InitResult) {
        Log.d(AudienceNetworkAds.TAG, result.message)
    }

    companion object {
        /**
         * It's recommended to call this method from Application.onCreate().
         * Otherwise you can call it from all Activity.onCreate()
         * methods for Activities that contain ads.
         *
         * @param context Application or Activity.
         */
        fun initialize(context: Context) {
            initialization(context, false, null)
        }

        fun initialize(context: Context, debuggable: Boolean) {
            initialization(context, debuggable, null)
        }

        fun initialize(context: Context, deviceId: String) {
            initialization(context, false, deviceId)
        }

        fun initialize(context: Context, debuggable: Boolean, deviceId: String) {
            initialization(context, debuggable, deviceId)
        }

        private fun initialization(context: Context, debuggable: Boolean, deviceId: String?) {
            if (!AudienceNetworkAds.isInitialized(context)) {
                if (debuggable) {
                    //DebugSettings.initialize(context)
                    AdSettings.turnOnSDKDebugger(context)
                    AdSettings.setTestMode(true)// for get test ad in your device
                    //AdSettings.setDebugBuild(true)
                    if (isNotEmpty(deviceId)) {
                        AdSettings.addTestDevice(deviceId)
                    }
                }
                AudienceNetworkAds
                    .buildInitSettings(context)
                    .withInitListener(AudienceNetworkInitializeHelper())
                    .initialize()
            }
        }
    }
}