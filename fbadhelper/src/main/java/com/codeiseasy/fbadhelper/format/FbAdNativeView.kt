package com.codeiseasy.fbadhelper.format

import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.facebook.ads.*
import com.codeiseasy.fbadhelper.R
import com.codeiseasy.fbadhelper.listener.FbNativeAdListener
import com.codeiseasy.fbadhelper.util.IndicatorHelper.loadingAnimationType

import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class FbAdNativeView : RelativeLayout {
    private var isAnimation: Boolean = false
    private var animationAccentColor: Int = -1
    private var backgroundViewColor: Int = -1

    private var mNativeBannerAdContainer: NativeAdLayout? = null
    private var adContainer: LinearLayout? = null
    private var animationView: LinearLayout? = null
    private var adChoicesContainer: LinearLayout? = null
    private var nativeSponsored: TextView? = null
    private var nativeAdTitle: TextView? = null
    private var nativeAdSocialContext: TextView? = null
    private var nativeAdBody: TextView? = null
    private var nativeAdMedia: MediaView? = null
    private var nativeAdIcon: MediaView? = null
    private var nativeAdCallToAction: Button? = null

    private var nativeAd: NativeAd? = null
    private var listener: FbNativeAdListener? = null
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var fanNativeTemplateStyle: FanNativeTemplateStyle? = null

    private var adUnitId: String? = null

    private var retryCount: Int = 0
    private var isRetryLoadAd: Boolean = false

    companion object {
        private const val MAX_NUMBER_OF_RETRIES = 3
    }

    constructor(context: Context?) : super(context) {
        initAttrs(context, null, 0, 0)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initAttrs(context, attrs, 0, 0)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttrs(context, attrs, defStyleAttr, 0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initAttrs(context, attrs, defStyleAttr, defStyleRes)
    }

    fun setAdUnitId(id: String?){
        this.adUnitId = id
    }

    @Deprecated("")
    fun setRetry(retry: Boolean) {
        this.isRetryLoadAd = retry
    }

    fun adListener(listener: FbNativeAdListener?){
        this.listener = listener
    }

    fun setTemplateStyle(style: FanNativeTemplateStyle?) {
        this.fanNativeTemplateStyle = style
    }

    fun loadAd() {
        if (TextUtils.isEmpty(this.adUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeUnitId before loadAd is called.")
        }
        nativeAd = NativeAd(context, this.adUnitId)
        nativeAd?.unregisterView()

        val adListener = object : NativeAdListener {
            override fun onAdLoaded(ad: Ad?) {
                listener?.onAdLoaded()
                // Race condition, load() called again before last ad was displayed
                if (nativeAd == null || nativeAd != ad) {
                    return
                }
                isAnimation = false
                viewsVisibility()
                // Inflate Native Ad into Container
                setNative(nativeAd)
            }

            override fun onError(ad: Ad?, adError: AdError?) {
                listener?.onError("Ad failed to load: " + adError!!.errorMessage)
                isAnimation = false
                viewsVisibility()
            }

            override fun onAdClicked(ad: Ad?) {
                listener?.onAdClicked()
            }

            override fun onLoggingImpression(ad: Ad?) {
                listener?.onLoggingImpression()
            }

            override fun onMediaDownloaded(ad: Ad?) {
                listener?.onMediaDownloaded()
            }
        }
        loadAd(adListener)
    }

    private fun loadAd(listener: NativeAdListener?) {
        // Request an ad
        nativeAd?.let { nonNullFbNative ->
            nonNullFbNative.loadAd(nonNullFbNative.buildLoadAdConfig().withAdListener(listener).build())
        }
    }

    private fun setNative(nativeAd: NativeAd?){
        nativeAd?.unregisterView()

        // Add the AdOptionsView
        val adOptionsView = AdOptionsView(context, nativeAd, mNativeBannerAdContainer)
        adChoicesContainer?.removeAllViews()
        adChoicesContainer?.addView(adOptionsView, 0)

        // Set the Text.
        nativeAdTitle?.text = nativeAd!!.advertiserName
        nativeSponsored?.text = nativeAd.sponsoredTranslation

        nativeAdSocialContext?.text = nativeAd.adSocialContext
        nativeAdBody?.text = nativeAd.adBodyText

        // Call To Action
        nativeAdCallToAction?.text = nativeAd.adCallToAction
        nativeAdCallToAction?.visibility = if (nativeAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE

        if (fanNativeTemplateStyle != null) {
            // Title
            if (fanNativeTemplateStyle?.titleTextColor != -1) {
                nativeAdTitle?.setTextColor(fanNativeTemplateStyle?.titleTextColor!!)
            }
            if (fanNativeTemplateStyle!!.titleTextSize != -1f) {
                nativeAdTitle?.textSize = fanNativeTemplateStyle?.titleTextSize!!
            }
            if (fanNativeTemplateStyle?.titleTextTypeface != null) {
                nativeAdTitle?.typeface = fanNativeTemplateStyle?.titleTextTypeface
            }

            // Sponsored
            if (fanNativeTemplateStyle?.sponsoredTextColor != -1) {
                nativeSponsored?.setTextColor(fanNativeTemplateStyle?.sponsoredTextColor!!)
            }
            if (fanNativeTemplateStyle!!.sponsoredTextSize != -1f) {
                nativeSponsored?.textSize = fanNativeTemplateStyle?.sponsoredTextSize!!
            }
            if (fanNativeTemplateStyle?.sponsoredTextTypeface != null) {
                nativeSponsored?.typeface = fanNativeTemplateStyle?.sponsoredTextTypeface
            }

            // Social Content
            if (fanNativeTemplateStyle?.socialContentTextColor != -1) {
                nativeAdSocialContext?.setTextColor(fanNativeTemplateStyle?.socialContentTextColor!!)
            }
            if (fanNativeTemplateStyle!!.socialContentTextSize != -1f) {
                nativeAdSocialContext?.textSize = fanNativeTemplateStyle?.socialContentTextSize!!
            }
            if (fanNativeTemplateStyle?.socialContentTextTypeface != null) {
                nativeAdSocialContext?.typeface = fanNativeTemplateStyle?.socialContentTextTypeface
            }

            // Body
            if (fanNativeTemplateStyle?.bodyTextColor != -1) {
                nativeAdBody?.setTextColor(fanNativeTemplateStyle?.bodyTextColor!!)
            }
            if (fanNativeTemplateStyle!!.bodyTextSize != -1f) {
                nativeAdBody?.textSize = fanNativeTemplateStyle?.bodyTextSize!!
            }
            if (fanNativeTemplateStyle?.bodyTextTypeface != null) {
                nativeAdBody?.typeface = fanNativeTemplateStyle?.bodyTextTypeface
            }

            // Call To Action
            if (fanNativeTemplateStyle?.callToActionTextColor != -1) {
                nativeAdCallToAction?.setTextColor(fanNativeTemplateStyle?.callToActionTextColor!!)
            }
            if (fanNativeTemplateStyle!!.callToActionTextSize != -1f) {
                nativeAdCallToAction?.textSize = fanNativeTemplateStyle?.callToActionTextSize!!
            }
            if (fanNativeTemplateStyle?.callToActionTextTypeface != null) {
                nativeAdCallToAction?.typeface = fanNativeTemplateStyle?.callToActionTextTypeface
            }
            if (fanNativeTemplateStyle?.callToActionBackgroundColor != null) {
                nativeAdCallToAction?.background = fanNativeTemplateStyle?.callToActionBackgroundColor
            }
        }

        // Create a list of clickable views
        val clickableViews: MutableList<View?> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(this)
        // Register the Title and CTA button to listen for clicks.
        nativeAd.registerViewForInteraction(this, nativeAdMedia, nativeAdIcon, clickableViews)
    }

    private fun initAttrs(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int){
        val typedArray = context!!.theme.obtainStyledAttributes(attrs, R.styleable.FbAdNativeView, defStyleAttr, defStyleRes)
        try {
            isAnimation = typedArray.getBoolean(R.styleable.FbAdNativeView_fnv_is_animation, false)
            animationAccentColor = typedArray.getColor(R.styleable.FbAdNativeView_fnv_animation_accent_color, -1)
            backgroundViewColor = typedArray.getColor(R.styleable.FbAdNativeView_fnv_bg_color, -1)
        } finally {
            typedArray.recycle()
        }
        setBackgroundColor(backgroundViewColor)
        initViews()
    }

    private fun initViews(){
        inflate(context, R.layout.fb_native_ad_layout, this)

        mNativeBannerAdContainer = findViewById(R.id.native_layout)

        adContainer = findViewById(R.id.ad_unit)
        animationView = findViewById(R.id.animation_view)

        adChoicesContainer = findViewById(R.id.ad_choices_container)
        // Create native UI using the ad metadata.
        nativeAdIcon = findViewById(R.id.native_icon_view)
        nativeAdTitle = findViewById(R.id.native_ad_title)
        nativeAdMedia = findViewById(R.id.native_ad_media)
        nativeAdSocialContext = findViewById(R.id.native_ad_social_context)
        nativeAdBody = findViewById(R.id.native_ad_body)
        nativeSponsored = findViewById(R.id.native_ad_sponsored_label)
        nativeAdCallToAction = findViewById(R.id.native_ad_call_to_action)
        avLoadingIndicatorView = findViewById(R.id.av_loading_indicator_view)

        avLoadingIndicatorView?.setIndicator(loadingAnimationType[Random().nextInt(loadingAnimationType.size)])
        avLoadingIndicatorView?.setIndicatorColor(animationAccentColor)
        viewsVisibility()
    }

    private fun viewsVisibility() {
        if (isAnimation) {
            animationView?.visibility = View.VISIBLE
            adContainer?.visibility = View.INVISIBLE
            return
        }
        animationView?.visibility = View.GONE
        adContainer?.visibility = View.VISIBLE
    }

    fun destroy(){
        nativeAdIcon?.destroy()
        nativeAdMedia?.destroy()
        nativeAd?.unregisterView()
        nativeAd?.destroy()
    }
}