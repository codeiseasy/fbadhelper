package com.codeiseasy.fbadhelper.format

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import com.codeiseasy.fbadhelper.AudienceNetworkHelper
import com.codeiseasy.fbadhelper.R
import com.codeiseasy.fbadhelper.listener.FbAdListener
import com.codeiseasy.fbadhelper.util.IndicatorHelper.loadingAnimationType
import com.facebook.ads.*
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class FbAdBannerView : LinearLayout {
    private var isAnimation: Boolean = false
    private var animationAccentColor: Int = -1
    private var backgroundViewColor: Int = Color.WHITE

    private var adContainer: LinearLayout? = null
    private var fbBannerAdView: AdView? = null
    private var fbBannerAdSize: AdSize = AdSize.BANNER_HEIGHT_50
    private var listener: FbAdListener? = null
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null

    private var fbBannerPlacementId: String? = null
    private var retryCount: Int = 0
    private var isRetryLoadAd: Boolean = false

    companion object {
        private const val MAX_NUMBER_OF_RETRIES = 3
    }

    constructor(context: Context?) : super(context!!) {
        initAttrs(context, null, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        initAttrs(context, attrs, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!,
        attrs,
        defStyleAttr
    ) {
        initAttrs(context, attrs, defStyleAttr, 0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context!!, attrs, defStyleAttr, defStyleRes) {
        initAttrs(context, attrs, defStyleAttr, defStyleRes)
    }

    override fun getOrientation(): Int {
        return HORIZONTAL
    }

    fun setBannerPlacementId(id: String?) {
        this.fbBannerPlacementId = id
    }

    fun setBannerAdViewSize(adSize: AdSize)  {
        this.fbBannerAdSize = adSize
    }

    @Deprecated("")
    fun setRetry(retry: Boolean) {
        this.isRetryLoadAd = retry
    }

    fun setAdView(adView: AdView?) {
        this.fbBannerAdView = adView
    }

    fun adListener(listener: FbAdListener?) {
        this.listener = listener
    }

    fun loadAd() {
        destroy()
        fbBannerAdView = null
        fbBannerAdView = AdView(context, this.fbBannerPlacementId, this.fbBannerAdSize)
        val adListener = object : AdListener {
            override fun onAdLoaded(ad: Ad?) {
                listener?.onAdLoaded()
                isAnimation = false
                viewsVisibility()
            }

            override fun onAdClicked(ad: Ad?) {
                listener?.onAdClicked()
            }

            override fun onLoggingImpression(ad: Ad?) {
                listener?.onLoggingImpression()
            }

            override fun onError(ad: Ad?, adError: AdError?) {
                listener?.onError("Ad failed to load: " + adError!!.errorMessage)
                isAnimation = false
                viewsVisibility()
            }
        }
        fbBannerAdView?.let { nonNullBannerAdView ->
            adContainer?.removeAllViews()
            adContainer?.addView(nonNullBannerAdView)
            nonNullBannerAdView.loadAd(nonNullBannerAdView.buildLoadAdConfig().withAdListener(adListener).build())
        }
    }

    private fun initAttrs(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        val typedArray = context!!.obtainStyledAttributes(
            attrs,
            R.styleable.FbAdBannerView,
            defStyleAttr,
            defStyleRes
        )
        try {
            isAnimation = typedArray.getBoolean(R.styleable.FbAdBannerView_fbn_is_animation, false)
            animationAccentColor =
                typedArray.getColor(R.styleable.FbAdBannerView_fbn_animation_accent_color, -1)
            backgroundViewColor = typedArray.getColor(R.styleable.FbAdBannerView_fbn_bg_color, -1)
        } finally {
            typedArray.recycle()
        }
        setBackgroundColor(backgroundViewColor)
        initViews()
    }

    private fun initViews() {
        inflate(context, R.layout.fb_banner_ad_unit, this)
        adContainer = findViewById(R.id.ad_unit)
        avLoadingIndicatorView = findViewById(R.id.av_loading_indicator_view)

        avLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random().nextInt(
                loadingAnimationType.size
            )]
        )
        avLoadingIndicatorView?.setIndicatorColor(animationAccentColor)
        viewsVisibility()
    }

    private fun viewsVisibility() {
        if (isAnimation) {
            avLoadingIndicatorView?.visibility = View.VISIBLE
            adContainer?.visibility = View.INVISIBLE
            return
        }
        avLoadingIndicatorView?.visibility = View.GONE
        adContainer?.visibility = View.VISIBLE
    }

    fun destroy() {
        if (fbBannerAdView != null) {
            fbBannerAdView?.destroy()
        }
    }
}