package com.codeiseasy.fbadhelper.format

import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import com.facebook.ads.*
import com.codeiseasy.fbadhelper.R
import com.codeiseasy.fbadhelper.listener.FbNativeAdListener
import com.codeiseasy.fbadhelper.util.IndicatorHelper.loadingAnimationType
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

class FbAdNativeBannerView : FrameLayout {
    private var isAnimation: Boolean = false
    private var animationAccentColor: Int = -1
    private var backgroundViewColor: Int = -1
    private var mNativeBannerAdContainer: NativeAdLayout? = null
    private var adContainer: LinearLayout? = null
    private var animationView: LinearLayout? = null
    private var adChoicesContainer: FrameLayout? = null
    private var nativeSponsored: TextView? = null
    private var nativeAdTitle: TextView? = null
    private var nativeAdSocialContext: TextView? = null
    private var nativeAdIconView: MediaView? = null
    private var nativeAdCallToAction: Button? = null

    private var nativeBannerAd: NativeBannerAd? = null
    private var listener: FbNativeAdListener? = null
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var fanNativeTemplateStyle: FanNativeTemplateStyle? = null

    private var adUnitId: String? = null

    private var retryCount: Int = 0
    private var isRetryLoadAd: Boolean = false

    companion object {
        private const val MAX_NUMBER_OF_RETRIES = 3
    }

    constructor(context: Context?) : super(context!!) {
        initAttrs(context, null, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        initAttrs(context, attrs, 0, 0)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!,
        attrs,
        defStyleAttr
    ) {
        initAttrs(context, attrs, defStyleAttr, 0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context!!, attrs, defStyleAttr, defStyleRes) {
        initAttrs(context, attrs, defStyleAttr, defStyleRes)
    }

    fun setAdUnitId(id: String?) {
        this.adUnitId = id
    }

    @Deprecated("")
    fun setRetry(retry: Boolean) {
        this.isRetryLoadAd = retry
    }

    fun adListener(listener: FbNativeAdListener?) {
        this.listener = listener
    }

    fun setTemplateStyle(style: FanNativeTemplateStyle?) {
        this.fanNativeTemplateStyle = style
    }

    fun loadAd() {
        if (TextUtils.isEmpty(this.adUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeUnitId before loadAd is called.")
        }
        nativeBannerAd = NativeBannerAd(context, this.adUnitId)
        nativeBannerAd?.unregisterView()

        val adListener = object : NativeAdListener {
            override fun onAdLoaded(ad: Ad?) {
                listener?.onAdLoaded()
                // Race condition, load() called again before last ad was displayed
                if (nativeBannerAd == null || nativeBannerAd != ad) {
                    return
                }
                isAnimation = false
                viewsVisibility()
                // Inflate Native Ad into Container
                setNative(nativeBannerAd)
            }

            override fun onError(ad: Ad?, adError: AdError?) {
                listener?.onError("Ad failed to load: " + adError!!.errorMessage)
                isAnimation = false
                viewsVisibility()
            }

            override fun onAdClicked(ad: Ad?) {
                listener?.onAdClicked()
            }

            override fun onLoggingImpression(ad: Ad?) {
                listener?.onLoggingImpression()
            }

            override fun onMediaDownloaded(ad: Ad?) {
                listener?.onMediaDownloaded()
            }
        }
        loadAd(adListener)
    }

    private fun loadAd(listener: NativeAdListener?) {
        // Request an ad
        nativeBannerAd?.let { nonNullFbNativeBanner ->
            nonNullFbNativeBanner.loadAd(
                nonNullFbNativeBanner.buildLoadAdConfig().withAdListener(listener).build()
            )
        }
    }

    private fun setNative(nativeBannerAd: NativeBannerAd?) {

        // Add the AdOptionsView
        val adOptionsView = AdOptionsView(
            context, nativeBannerAd,
            mNativeBannerAdContainer,
            AdOptionsView.Orientation.HORIZONTAL,
            20
        )
        adChoicesContainer?.removeAllViews()
        adChoicesContainer?.addView(adOptionsView, 0)

        // Set the Text.
        nativeAdTitle?.text = nativeBannerAd!!.advertiserName
        nativeSponsored?.text = nativeBannerAd.sponsoredTranslation

        nativeAdSocialContext?.text = nativeBannerAd.adSocialContext

        // Call To Action
        nativeAdCallToAction?.text = nativeBannerAd.adCallToAction
        nativeAdCallToAction?.visibility =
            if (nativeBannerAd.hasCallToAction()) View.VISIBLE else View.INVISIBLE

        if (fanNativeTemplateStyle != null) {
            // Call To Action
            if (fanNativeTemplateStyle?.callToActionTextColor != -1) {
                nativeAdCallToAction?.setTextColor(fanNativeTemplateStyle?.callToActionTextColor!!)
            }

            if (fanNativeTemplateStyle!!.callToActionTextSize != -1f) {
                nativeAdCallToAction?.textSize = fanNativeTemplateStyle?.callToActionTextSize!!
            }
            if (fanNativeTemplateStyle?.callToActionTextTypeface != null) {
                nativeAdCallToAction?.typeface = fanNativeTemplateStyle?.callToActionTextTypeface
            }

            if (fanNativeTemplateStyle?.callToActionBackgroundColor != null) {
                nativeAdCallToAction?.background = fanNativeTemplateStyle?.callToActionBackgroundColor
            }

            // Title
            if (fanNativeTemplateStyle?.titleTextColor != -1) {
                nativeAdTitle?.setTextColor(fanNativeTemplateStyle?.titleTextColor!!)
            }
            if (fanNativeTemplateStyle!!.titleTextSize != -1f) {
                nativeAdTitle?.textSize = fanNativeTemplateStyle?.titleTextSize!!
            }
            if (fanNativeTemplateStyle?.titleTextTypeface != null) {
                nativeAdTitle?.typeface = fanNativeTemplateStyle?.titleTextTypeface
            }

            // Sponsored
            if (fanNativeTemplateStyle?.sponsoredTextColor != -1) {
                nativeSponsored?.setTextColor(fanNativeTemplateStyle?.sponsoredTextColor!!)
            }
            if (fanNativeTemplateStyle!!.sponsoredTextSize != -1f) {
                nativeSponsored?.textSize = fanNativeTemplateStyle?.sponsoredTextSize!!
            }
            if (fanNativeTemplateStyle?.sponsoredTextTypeface != null) {
                nativeSponsored?.typeface = fanNativeTemplateStyle?.sponsoredTextTypeface
            }

            // Social Content
            if (fanNativeTemplateStyle?.socialContentTextColor != -1) {
                nativeAdSocialContext?.setTextColor(fanNativeTemplateStyle?.socialContentTextColor!!)
            }
            if (fanNativeTemplateStyle!!.socialContentTextSize != -1f) {
                nativeAdSocialContext?.textSize = fanNativeTemplateStyle?.socialContentTextSize!!
            }
            if (fanNativeTemplateStyle?.socialContentTextTypeface != null) {
                nativeAdSocialContext?.typeface = fanNativeTemplateStyle?.socialContentTextTypeface
            }
        }
        // Create a list of clickable views
        val clickableViews: MutableList<View?> = ArrayList()
        clickableViews.add(nativeAdTitle)
        clickableViews.add(nativeAdCallToAction)
        clickableViews.add(this)
        // Register the Title and CTA button to listen for clicks.
        nativeBannerAd.registerViewForInteraction(this, nativeAdIconView, clickableViews)
    }

    private fun initAttrs(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        val typedArray = context!!.obtainStyledAttributes(
            attrs,
            R.styleable.FbAdNativeBannerView,
            defStyleAttr,
            defStyleRes
        )
        try {
            isAnimation =
                typedArray.getBoolean(R.styleable.FbAdNativeBannerView_fnb_is_animation, false)
            animationAccentColor =
                typedArray.getColor(R.styleable.FbAdNativeBannerView_fnb_animation_accent_color, -1)
            backgroundViewColor =
                typedArray.getColor(R.styleable.FbAdNativeBannerView_fnb_bg_color, -1)
        } finally {
            typedArray.recycle()
        }
        setBackgroundColor(backgroundViewColor)
        initViews()
    }

    private fun initViews() {
        inflate(context, R.layout.fb_native_banner_ad_unit, this)

        mNativeBannerAdContainer = findViewById(R.id.native_layout)

        adContainer = findViewById(R.id.ad_unit)
        animationView = findViewById(R.id.animation_view)

        // Create native UI using the ad metadata.
        nativeAdTitle = findViewById(R.id.native_ad_title)
        nativeAdSocialContext = findViewById(R.id.native_ad_social_context)
        nativeSponsored = findViewById(R.id.native_ad_sponsored_label)
        nativeAdIconView = findViewById(R.id.native_icon_view)
        nativeAdCallToAction = findViewById(R.id.native_ad_call_to_action)
        adChoicesContainer = findViewById(R.id.ad_choices_container)


        avLoadingIndicatorView = findViewById(R.id.av_loading_indicator_view)
        avLoadingIndicatorView?.setIndicator(
            loadingAnimationType[Random().nextInt(
                loadingAnimationType.size
            )]
        )
        avLoadingIndicatorView?.setIndicatorColor(animationAccentColor)
        viewsVisibility()
    }

    private fun viewsVisibility() {
        if (isAnimation) {
            animationView?.visibility = View.VISIBLE
            adContainer?.visibility = View.INVISIBLE
            return
        }
        animationView?.visibility = View.GONE
        adContainer?.visibility = View.VISIBLE
    }

    fun destroy() {
        nativeAdIconView?.destroy()
        nativeBannerAd?.unregisterView()
        nativeBannerAd?.destroy()
    }
}