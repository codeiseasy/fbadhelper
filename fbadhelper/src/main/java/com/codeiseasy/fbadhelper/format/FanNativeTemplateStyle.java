package com.codeiseasy.fbadhelper.format;


import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;

/**
 * A class containing the optional styling options for the Native Template. *
 */
public class FanNativeTemplateStyle {

    // Call to action typeface.
    private Typeface callToActionTextTypeface;

    // Size of call to action text.
    private float callToActionTextSize = -1f;

    // Call to action text color. in the form 0xAARRGGBB.
    private int callToActionTextColor = -1;

    // Call to action background color.
    private ColorDrawable callToActionBackgroundColor;


    // All templates have a primary text area which is populated by the native ad's headline.

    // Title text typeface.
    private Typeface titleTextTypeface;

    // Size of title text.
    private float titleTextSize = -1f;

    // Title text color. in the form 0xAARRGGBB.
    private int titleTextColor = -1;

    // The typeface, typeface color, and background color for the second row of text in the template.
    // All templates have a secondary text area which is populated either by the body of the ad or
    // by the rating of the app.

    // Sponsored text typeface.
    private Typeface sponsoredTextTypeface;

    // Size of sponsored text.
    private float sponsoredTextSize = -1f;

    // Sponsored text color. in the form 0xAARRGGBB.
    private int sponsoredTextColor = -1;

    // The typeface, typeface color, and background color for the third row of text in the template.
    // The third row is used to display store name or the default tertiary text.

    // Social Content text typeface.
    private Typeface socialContentTextTypeface;

    // Size of social content text.
    private float socialContentTextSize = -1f;

    // Social Content text color. in the form 0xAARRGGBB.
    private int socialContentTextColor = -1;


    // Body text typeface.
    private Typeface bodyTextTypeface;

    // Size of body text.
    private float bodyTextSize = -1f;

    // Body text color. in the form 0xAARRGGBB.
    private int bodyTextColor = -1;


    // The background color for the bulk of the ad.
    private ColorDrawable mainBackgroundColor;

    public Typeface getCallToActionTextTypeface() {
        return callToActionTextTypeface;
    }

    public float getCallToActionTextSize() {
        return callToActionTextSize;
    }

    public ColorDrawable getCallToActionBackgroundColor() {
        return this.callToActionBackgroundColor;
    }

    public int getCallToActionTextColor() {
        return this.callToActionTextColor;
    }

    public Typeface getTitleTextTypeface() {
        return this.titleTextTypeface;
    }

    public float getTitleTextSize() {
        return this.titleTextSize;
    }

    public int getTitleTextColor() {
        return this.titleTextColor;
    }

    public Typeface getSponsoredTextTypeface() {
        return this.sponsoredTextTypeface;
    }

    public float getSponsoredTextSize() {
        return this.sponsoredTextSize;
    }

    public int getSponsoredTextColor() {
        return this.sponsoredTextColor;
    }

    public Typeface getSocialContentTextTypeface() {
        return this.socialContentTextTypeface;
    }


    public float getSocialContentTextSize() {
        return this.socialContentTextSize;
    }

    public int getSocialContentTextColor() {
        return this.socialContentTextColor;
    }

    public Typeface getBodyTextTypeface() {
        return bodyTextTypeface;
    }

    public float getBodyTextSize() {
        return bodyTextSize;
    }

    public int getBodyTextColor() {
        return bodyTextColor;
    }

    public ColorDrawable getMainBackgroundColor() {
        return this.mainBackgroundColor;
    }

    /**
     * A class that provides helper methods to build a style object. *
     */
    public static class Builder {

        private FanNativeTemplateStyle styles;

        public Builder() {
            this.styles = new FanNativeTemplateStyle();
        }

        public FanNativeTemplateStyle.Builder withCallToActionTextTypeface(Typeface callToActionTextTypeface) {
            this.styles.callToActionTextTypeface = callToActionTextTypeface;
            return this;
        }

        public FanNativeTemplateStyle.Builder withCallToActionTextSize(float callToActionTextSize) {
            this.styles.callToActionTextSize = callToActionTextSize;
            return this;
        }

        public FanNativeTemplateStyle.Builder withCallToActionTextColor(int callToActionTextColor) {
            this.styles.callToActionTextColor = callToActionTextColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withCallToActionBackgroundColor(ColorDrawable callToActionBackgroundColor) {
            this.styles.callToActionBackgroundColor = callToActionBackgroundColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withTitleTextTypeface(Typeface titleTextTypeface) {
            this.styles.titleTextTypeface = titleTextTypeface;
            return this;
        }

        public FanNativeTemplateStyle.Builder withTitleTextSize(float titleTextSize) {
            this.styles.titleTextSize = titleTextSize;
            return this;
        }

        public FanNativeTemplateStyle.Builder withTitleTextColor(int titleTextColor) {
            this.styles.titleTextColor = titleTextColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withSponsoredTextTypeface(Typeface sponsoredTextTypeface) {
            this.styles.sponsoredTextTypeface = sponsoredTextTypeface;
            return this;
        }

        public FanNativeTemplateStyle.Builder withSponsoredTextSize(float sponsoredTextSize) {
            this.styles.sponsoredTextSize = sponsoredTextSize;
            return this;
        }

        public FanNativeTemplateStyle.Builder withSponsoredTextColor(int sponsoredTextColor) {
            this.styles.sponsoredTextColor = sponsoredTextColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withSocialContentTextTypeface(Typeface socialContentTextTypeface) {
            this.styles.socialContentTextTypeface = socialContentTextTypeface;
            return this;
        }

        public FanNativeTemplateStyle.Builder withSocialContentTextSize(float socialContentTextSize) {
            this.styles.socialContentTextSize = socialContentTextSize;
            return this;
        }


        public FanNativeTemplateStyle.Builder withSocialContentTextColor(int socialContentTextColor) {
            this.styles.socialContentTextColor = socialContentTextColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withBodyTextTypeface(Typeface bodyTextTypeface) {
            this.styles.bodyTextTypeface = bodyTextTypeface;
            return this;
        }

        public FanNativeTemplateStyle.Builder withBodyTextSize(float bodyTextSize) {
            this.styles.bodyTextSize = bodyTextSize;
            return this;
        }

        public FanNativeTemplateStyle.Builder withBodyTextColor(int bodyTextColor) {
            this.styles.bodyTextColor = bodyTextColor;
            return this;
        }

        public FanNativeTemplateStyle.Builder withMainBackgroundColor(ColorDrawable mainBackgroundColor) {
            this.styles.mainBackgroundColor = mainBackgroundColor;
            return this;
        }

        public FanNativeTemplateStyle build() {
            return styles;
        }
    }
}
