package com.codeiseasy.fbadhelper.listener

import com.facebook.ads.Ad
import com.facebook.ads.AdError

interface FbAdListener {
    fun onAdLoaded() {}
    fun onAdClicked() {}
    fun onLoggingImpression() {}
    fun onError(adError: String?) {}
}