package com.codeiseasy.fbadhelper.listener

import com.facebook.ads.Ad
import com.facebook.ads.AdError

interface FbNativeAdListener {
    fun onAdLoaded() {}
    fun onAdClicked() {}
    fun onLoggingImpression() {}
    fun onMediaDownloaded() {}
    fun onError(adError: String?) {}
}