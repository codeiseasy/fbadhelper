package com.codeiseasy.fbadhelper.listener

import com.facebook.ads.Ad
import com.facebook.ads.AdError

interface FbInterstitialAdListener {
    fun onInterstitialDisplayed() {} // Interstitial ad displayed callback
    fun onInterstitialDismissed() {} // Interstitial dismissed callback
    fun onError(adError: String?) {} // Ad error callback
    fun onAdLoaded() {} // Interstitial ad is loaded and ready to be displayed
    fun onAdClicked() {} // Ad clicked callback
    fun onLoggingImpression() {} // Ad impression logged callback
}