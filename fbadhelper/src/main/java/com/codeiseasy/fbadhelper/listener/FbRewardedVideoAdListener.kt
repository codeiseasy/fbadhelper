package com.codeiseasy.fbadhelper.listener

import com.facebook.ads.Ad
import com.facebook.ads.AdError

interface FbRewardedVideoAdListener {
    fun onAdLoaded() {}
    fun onAdClicked() {}
    fun onRewardedVideoClosed() {}
    fun onRewardedVideoCompleted() {}
    fun onLoggingImpression() {}
    fun onError(adError: String?) {}
}