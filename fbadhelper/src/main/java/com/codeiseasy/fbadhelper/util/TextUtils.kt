package com.codeiseasy.fbadhelper.util

import android.text.TextUtils
import java.util.regex.Matcher
import java.util.regex.Pattern

object TextUtils {
    fun isEmpty(text: String?): Boolean{
        return  text == null ||
                TextUtils.isEmpty(text) ||
                text.isEmpty() ||
                text.isBlank() ||
                text.isNullOrBlank() ||
                text.isNullOrEmpty() ||
                text.equals("null", true)
    }

    fun isNotEmpty(text: String?): Boolean{
        return  text != null  ||
                !TextUtils.isEmpty(text) ||
                text.toString().isNotEmpty() ||
                text.toString().isNotBlank() ||
                !text.equals("null", true)
    }

    fun isEmailValidate(email: String?): Boolean{
        return emailValidator(email)
    }

    /**
     * validate your email address format. Ex-akhi@mani.com
     */
    private fun emailValidator(email: String?): Boolean {
        val regex = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }
}