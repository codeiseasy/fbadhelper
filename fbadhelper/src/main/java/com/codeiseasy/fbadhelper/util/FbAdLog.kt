package com.codeiseasy.fbadhelper.util

import android.util.Log
import com.facebook.ads.BuildConfig

import java.io.IOException


/**
 * Created by Develop on 11/21/2017.
 */

object FbAdLog {

    //TODO Logcat -> VERBOSE
    fun v(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg.toString())
        }
    }

    //TODO Logcat -> DEBUG
    fun d(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg.toString())
        }
    }

    //TODO Logcat -> INFO
    fun i(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg.toString())
        }
    }

    //TODO Logcat -> WARN
    fun w(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, msg.toString())
        }
    }

    //TODO Logcat -> ERROR
    fun e(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg.toString())
        }
    }
    //TODO Logcat -> ERROR > IOException
    fun e(tag: String?, msg: String?, e: IOException) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg.toString(), e)
        }
    }

    //TODO Logcat -> ERROR > Throwable
    fun e(tag: String?, msg: String?, e: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg.toString(), e)
        }
    }

    fun printStackTrace(e: Exception) {
        e.printStackTrace()
    }
}

