package com.codeiseasy.fbadhelper.util

/**
 * Created by Develop on 12/9/2017.
 */

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.util.Log

/**
 * Created by juma on 18/04/17.
 */
@SuppressLint("MissingPermission")
class CheckNetwork {
    private var connectivityManager: ConnectivityManager? = null
    private var connected = false

    val isOnline: Boolean
        get() {
            try {
                connectivityManager = mContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager?.activeNetworkInfo
                connected = networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
                return connected
            } catch (e: Exception) {
                println("CheckConnectivity Exception: " + e.message)
                Log.v("connectivity", e.toString())
            }

            return connected
        }

    val isOffline: Boolean
        get() {
            try {
                connectivityManager = mContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager?.activeNetworkInfo
                connected = networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
                return !connected
            } catch (e: Exception) {
                println("CheckConnectivity Exception: " + e.message)
                Log.v("connectivity", e.toString())
            }

            return !connected
        }

    companion object {
        private var mContext: Context? = null
        /**
         * We use this class to determine if the application has been connected to either WIFI Or Mobile
         * Network, before we make any network request to the server.
         *
         *
         * The class uses two permission - INTERNET and ACCESS NETWORK STATE, to determine the user's
         * connection stats
         */

        private val mInstance = CheckNetwork()

        fun getInstance(ctx: Context?): CheckNetwork {
            mContext = ctx
            return mInstance
        }
    }
}