package com.codeiseasy.fbadhelper.util

object IndicatorHelper {
    val loadingAnimationType = arrayOf(
        "SquareSpinIndicator",
        "LineScalePulseOutRapidIndicator",
        "BallClipRotatePulseIndicator",
        "BallSpinFadeLoaderIndicator",
        "PacmanIndicator",
        "BallClipRotatePulseIndicator",
        "LineScalePartyIndicator"
    )
}