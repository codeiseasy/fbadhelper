package com.codeiseasy.fbadhelper.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar


import androidx.core.graphics.drawable.DrawableCompat
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.codeiseasy.fbadhelper.R


class UILoadingDialog {
    private var context: Context? = null
    private var dialog: Dialog? = null
    private var progressBar: ProgressBar? = null
    private var linearLayout: LinearLayout? = null
    private var textView: TextView? = null
    private var displayMetrics: DisplayMetrics
    private var isCustomView: Boolean = false
    private var isCancelable: Boolean = true
    private var bgColor: Int = Color.WHITE
    private lateinit var view: View
    private var radius: Float = 0f
    private var bodyText: String? = null

    constructor(context: Context?) {
        this.context = context
        this.displayMetrics = context!!.resources.displayMetrics
    }

    constructor(context: Context?, cancelable: Boolean) {
        this.context = context
        this.isCancelable = cancelable
        this.displayMetrics = context!!.resources.displayMetrics
    }

    companion object {
        fun init(context: Context?, cancelable: Boolean) : UILoadingDialog {
            return UILoadingDialog(
                context,
                cancelable
            )
        }

        fun init(context: Context?) : UILoadingDialog {
            return UILoadingDialog(context)
        }
    }

     fun build() : UILoadingDialog {
         dialog = Dialog(context!!, R.style.fbad_dialog_style)
         dialog?.setCancelable(isCancelable)
         dialog!!.window!!.requestFeature(1)
         dialog!!.window!!.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
         when(isCustomView){
             true -> dialog?.setContentView(view)
             false -> dialog?.setContentView(views())
         }
        return this
    }

    fun cancelable(cancelable: Boolean): UILoadingDialog {
        this.isCancelable = cancelable
        return this
    }

    fun setText(text: String?) : UILoadingDialog {
        this.bodyText = text
        return this
    }

    fun backgroundColor(color: Int): UILoadingDialog {
        this.bgColor = color
        return this
    }

    fun cornerRadius(radius: Float): UILoadingDialog {
        this.radius = radius
        return this
    }

    fun view(viewGroup: ViewGroup): UILoadingDialog {
        if(viewGroup != null){
            isCustomView = true
        }
        view = viewGroup
        return this
    }

    fun view(viewGroup: View): UILoadingDialog {
        if(viewGroup != null){
            isCustomView = true
        }
        view = viewGroup
        return this
    }

     private fun views() : View {
         linearLayout = LinearLayout(context)
         progressBar = ProgressBar(context)
         textView = TextView(context)

         var linearParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
         linearParams.gravity = Gravity.CENTER
         linearLayout?.layoutParams = linearParams
         linearLayout?.orientation = LinearLayout.VERTICAL
         linearLayout?.gravity = Gravity.CENTER
         linearLayout?.background = UIDrawableVector.setupShape(context).padding(8f).cornerRadius(radius).backgroundColor(bgColor).apply()


         var progressParams = LinearLayout.LayoutParams(UIDrawableVector(context).dimension<Int>(50f), UIDrawableVector(context).dimension<Int>(50f))
         progressBar?.layoutParams = progressParams
         progressBar?.indeterminateDrawable = ContextCompat.getDrawable(context!!, R.drawable.fbad_progress_drawable)
         val progressDrawable = progressBar?.indeterminateDrawable
         if (progressDrawable != null) {
             val mutateDrawable = progressDrawable.mutate()
             DrawableCompat.setTint(mutateDrawable, Color.WHITE)
             progressBar?.progressDrawable = mutateDrawable
         }

         var textParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
         textParams.topMargin = 10
         textView?.layoutParams = textParams
         textView?.text = bodyText.toString() //"Checking EEA..."
         textView?.setTextColor(Color.WHITE)

         linearLayout?.addView(progressBar)
         linearLayout?.addView(textView)
         return linearLayout!!
    }

     fun show(){
        if (dialog != null){
            dialog?.show()
        }
    }

     fun hide(){
        if (dialog!!.isShowing){
            dialog?.dismiss()
            //dialog?.cancel()
        }
    }

    fun window(): Window? {
        return dialog!!.window
    }
}
